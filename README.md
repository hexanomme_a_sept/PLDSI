# Ressources 

Lien de la vidéo de présentation : http://mediacenter.insa-lyon.fr/videos/?video=MEDIA131218081959359

Moodle : http://moodle2.insa-lyon.fr/course/view.php?id=1721

# Outils

Gestion des fichiers : ... Vous êtes dessus

Gestion de projet/de travail : http://magentamine.ddns.net/

Édition du rapport : Latex (oui j'insiste) (moi aussi, et s'il y a une présentation, ça sera un beamer)

# Livrables 

## Initialisation

    Dossier initialisation
   
## Qualité

    Plan Assurance Qualité
    Suivi Qualité
    
## Rendu

    Présentation
    Dossier d'expression des besoin
    Dossier d'expression des solutions/D'évaluation des solutions
    Dossier Bilan
    
## Suivi

    Dossier de suivi hebdomadaire
    Planning Prévisionnel
    Planning de réalisation
    Tableau de Bord (Humeur + livrables)

# Qui fait quoi ?

## Chef de projet
    
    Anthony
    
## Responsable qualité
    
    Franck
    
## Responsable modélisation

    Felix
    
## Expert ARIS

    Afanasie
    
## Expert ERP

    Mohamed
    
## Expert metiers

    Daniela
    
## Experts bullshit

    Nous tous